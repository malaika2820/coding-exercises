#include <iostream>
#include <vector>
#include <cmath>

using namespace std;

int prod_py_triplets(int n) {
	
	int product;
	for (int a = 1; a < n/2; a++) {
	       for (int b = 1; b < a; b++) { 
			float c = sqrt(a * a + b * b);
		 	if (c != int(c))
				continue;
			if (a + b + c == 1000) {
				product = a * b * c;
				break;
			}
	       }
	}	       
	
	return product;
}

int main(int argc, char* argv[]) {

	int n = 1000;
	cout << prod_py_triplets(n) << endl;

	return 0;
}	
