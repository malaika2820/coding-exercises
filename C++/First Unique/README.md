### Problem Description
Given an array, find the first integer which is unique in the array. Unique means the number does not repeat and appears only once in the whole array. 

### Problem Statement 
Implement a function, findFirstUnique(int arr[], int size) which takes an array and its size as input and returns the first unique integer in the array. The function returns -1 if no unique number is found.

### Input: 
An array of integers and its size

### Output: 
The first unique element in the array

### Sample Input 
6

[2,3,9,2,3,6]

### Sample Output 
9