#include <iostream>
#include <unordered_map>

using namespace std;

int first_unique(int arr[], int size) {

	unordered_map<int, int> freq;

	for (int i = 0; i < size; i++) {
		freq[arr[i]]++;
	}

	for (int i = 0; i < size; i++) {
		if (freq[arr[i]] == 1)
			return arr[i];
	}

	return -1;
}

int main(int argc, char* argv[]) {

	int size;
    cin >> size;

	int* arr = new int[size];

	for (int i = 0; i < size; i++) {
		cin >> arr[i];
	}

	cout << first_unique(arr, size) << endl;

	return 0;
}
