#include <iostream>
#include <vector>

using namespace std;

vector<int> fibonacci(int n) {

	vector<int> fib = {1, 2};
	int a = 1;
	int b = 2;
	int c = 0;

	while (c < n) {
		
		c = a + b;
		a = b;
		b = c;
		fib.push_back(c);
	}

	return fib;
}

int sum_even(int n) {

	vector<int> fib = fibonacci(n);
	int total = 0;
	
	for (const int i : fib) {
	
		if (i % 2 == 0)
			total += i;
	}

	return total;
}

int main(int argc, char* argv[]) {

	int n = stoi(argv[1]);
	cout << sum_even(n) << endl;

	return 0;
}
