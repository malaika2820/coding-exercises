#include <iostream>

using namespace std;

int* mergeArrays(int arr1[], int arr2[], int size1, int size2)
{
    int* arr3 = new int[size1 + size2];
    int i = 0, j = 0, k = 0;
    
    while (i < size1 && j < size2) { 
        if (arr1[i] < arr2[j]) 
            arr3[k++] = arr1[i++]; 
        else
            arr3[k++] = arr2[j++]; 
    }
    while (i < size1) 
        arr3[k++] = arr1[i++]; 
  
    while (j < size2) 
        arr3[k++] = arr2[j++]; 
    
    return arr3;
}

int main(int argc, char* argv[]) {
    
    int size1, size2;
    
    cin >> size1;
    int arr1[size1];
    for (int i = 0; i < size1; i++) {
        cin >> arr1[i];
    }

    cin >> size2;
    int arr2[size2];
    for (int i = 0; i < size2; i++) {
        cin >> arr2[i];
    }

    int* result = mergeArrays(arr1, arr2, size1, size2); // calling mergeArrays
    for (int i = 0; i < size1 + size2; i++) {
        cout << result[i] << " ";
    }
    return 0;
}