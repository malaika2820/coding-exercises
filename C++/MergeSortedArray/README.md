### Problem Description
Implement a function mergeArrays(int arr1[], int arr2[], int arr1Size,int arr2Size)  which merges two sorted arrays into another sorted array. 

### Input:
Two sorted arrays and their sizes.

### Output: 
A merged sorted array consisting of all elements of both input arrays.

### Sample Input: 
4

[1,3,4,5]  

4

[2,6,7,8]

### Sample Output: 
[1,2,3,4,5,6,7,8]