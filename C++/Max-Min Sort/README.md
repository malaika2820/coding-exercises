### Problem Description
Arrange elements in such a way that the maximum element appears at first, then the minimum at second, then the second maximum at the third position, and second minimum at fourth and so on.

### Problem Statement 
Implement a function maxMin(int arr[], int size) which takes a sorted array arr and its size and will re-arrange the elements of a sorted array such that the first position will have the largest number, the second will have the smallest, and the third will have the second-largest and so on. In other words, all the even-numbered indices will have the largest numbers in the array in descending order and the odd-numbered indices will have the smallest numbers in ascending order.
The given array is sorted in ascending order.

### Input: 
A sorted array and its size

### Output: 
A list with elements stored in max/min form

### Sample Input: 
5

[1,2,3,4,5]

### Sample Output 
[5,1,4,2,3]