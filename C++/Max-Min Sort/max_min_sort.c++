#include <iostream>

using namespace std;

//O(n ^ 2) time
int* max_min(int arr[], int size) {

	for (int i = 0; i < size; i += 2) {

		int m = arr[i];
		arr[i] = arr[size - 1];

		for (int j = size - 1; j > i + 1; j--) {
			arr[j] = arr[j - 1];
		}

		arr[i + 1] = m;
	}

	return arr;
}

//O(n) + auxiliary space
int* max_min_2(int arr[], int size) {

	int* result = new int[size];
	int index = 0;
	int i = 0;

	while (index < size) {
		result[index++] = arr[size - i - 1];
		result[index++] = arr[i];
		i++;
	}

	return result;
}

//O(n) + O(1) space
int* max_min_3(int arr[], int size) {

	int max_index = size - 1;
	int min_index = 0;

	int max_num = arr[max_index] + 1;

	for (int i = 0; i < size; i++) {
		
		if (i % 2 == 0) {
			arr[i] += (arr[max_index] % max_num) * max_num;
			max_index--;
		}
		else {
			arr[i] += (arr[min_index] % max_num) * max_num;
			min_index++;
		}
	}

	for (int i = 0; i < size; i++) {
		arr[i] = arr[i] / max_num;
	}

	return arr;
}
		        	
int main(int argc, char* argv[]) {

	int size = stoi(argv[1]);

	int* arr = new int[size];

	for (int i = 0; i < size; i++) {
		cin >> arr[i];
	}

	int* res = max_min_3(arr, size);

	for (int i = 0; i < size; i++) {
		cout << res[i] << " ";
	}

	cout << endl;

	return 0;
}
