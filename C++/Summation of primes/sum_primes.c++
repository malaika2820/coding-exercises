#include <iostream>

using namespace std;

bool is_prime(int n) {
	
	if (n == 2 || n == 3)
		return true;

	if (n <= 1 || n % 2 == 0 || n % 3 == 0)
		return false;
	
	int r = 5;

	while (r * r <= n) {
		if (n % r == 0)
			return false;
		r +=2;
		if (n % r == 0)
			return false;
		r += 4;
	}
	return true;
}

long long sum_primes(int n) {
	
	long long total = 2;

	for (int i = 3; i < n; i+= 2) {
		if (is_prime(i))
			total += i;
	}
	return total;
}

int main(int argc, char* argv[]) {

	//int n = 2000000;
	int n = stoi(argv[1]);
	cout << sum_primes(n) << endl;

	return 0;
}
