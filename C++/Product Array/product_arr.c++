#include <iostream>

using namespace std;

int* find_product(int arr[], int size) {

	int p = 1;
	int zero_count = 0;

	for (int i = 0; i < size; i++) {
		if (arr[i] != 0)
			p*= arr[i];
		else
			zero_count++;
	}

	for (int i = 0; i < size; i++) {
		if (zero_count >= 2)
			arr[i] = 0;
		
		else if (zero_count == 1) {
			if (arr[i] != 0) 
				arr[i] = 0;
			else
				arr[i] = p;
		}
		
		else
			arr[i] = p / arr[i];
	}

	return arr;
}

int main(int argc, char* argv[]) {

	int size = stoi(argv[1]);

	int *arr = new int [size];

	for (int i = 0; i < size; i++) {
		cin >> arr[i];
	}

	int *res = find_product(arr, size);

	for (int i = 0; i < size; i++) {
		cout << arr[i] << " ";
	}
	cout << endl;

	return 0;
}
