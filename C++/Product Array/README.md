### Problem Description
Given an array, return an array where each index stores the product of all numbers in the array except the number at the index itself.

### Problem Statement 
Implement a function, findProduct(int arr[], int size) which takes an array arr and its size as an input and returns an array so that each index has a product of all the numbers present in the array except the number stored at that index.

### Constraints
Size should be equal to or greater than 2.

### Input: 
An array of integers and its size

### Output: 
An array such that each index has a product of all the numbers in the array except the number stored at that index

### Sample Input: 
4

[1,2,3,4]

### Sample Output: 
[24,12,8,6]


