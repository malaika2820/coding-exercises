#include <iostream>

using namespace std;

int* re_arrange(int arr[], int size) {

	int* result = new int[size];
	int index = 0;

	for (int i = 0; i < size; i++) {
		if (arr[i] < 0)
			result[index++] = arr[i];
	}

	for (int i = 0; i < size; i++) {
		if (arr[i] >= 0)
			result[index++] = arr[i];
	}

	return result;
}

int main(int argc, char* argv[]) {

	int size;
    cin >> size;

	int* arr = new int[size];
	for (int i = 0; i < size; i++) {
		cin >> arr[i];
	}

	arr = re_arrange(size, arr);

	for (int i = 0; i < size; i++) {
		cout << arr[i] << " ";
	}
	
	cout << endl;
	return 0;
}
