### Problem Description
Given an array, re-arrange its elements in such a way that the negative elements appear at one side and positive elements appear in the other.

### Problem Statement 
Implement a function reArrange(int arr[], int size) which takes an array arr and its size as input and rearranges the elements such that all the negative elements appear on the left and positive elements appear at the right.
Consider 0 as a positive number.

### Input: 
An array of integers and its size

### Output: 
A rearranged array with negative elements at the left and positive elements at the right

### Sample Input:
7

[10,-1,20,4,5,-9,-6]

### Sample Output 
[-1,-9,-6,10,20,4,5]
