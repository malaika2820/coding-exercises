#include <iostream>

using namespace std;

bool is_prime(int n) {

	if (n % 2 == 0 || n % 3 == 0 || n <= 1)
		return false;
	
	int r = 5;
	while (r * r <= n) {
		if (n % r == 0)
			return false;
		r += 2;
		if (n % r == 0)
			return false;
		r += 4;
	}
	return true;
}

long long nth_prime(int n) {

	int count = 3;
	int i = 5;

	while (count < n) {

		i++;
		if (is_prime(i))
			count++;
	}

	return i;
}

int main(int argc, char* argv[]) {

	//int n = 10001;
	int n = stoi(argv[1]);
	cout << nth_prime(n) << endl;

	return 0;
}
