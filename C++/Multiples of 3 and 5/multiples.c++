#include <iostream>

using namespace std;

int sum_multiples(int n);

int main(int argc, char* argv[]) {

	int n = stoi(argv[1]);
	cout << sum_multiples(n) << endl;

	return 0;
}

int sum_multiples(int n) {

	int total = 0;
	for (int i = 3; i < n; i++) {
		if (i % 3 == 0 || i % 5 == 0)
			total += i;
	}

	return total;
}
