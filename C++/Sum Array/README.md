### Problem Description
Given an array and a number value, find two numbers from an array that sum to the value.

### Problem Statement 
Implement a function findSum(int arr[], int value, int size) which takes an array arr, a number value and size of the array as input and returns an array of two numbers that add up to value. In case there is more than one pair in the array containing numbers that add up to value, you are required to return only one such pair. If no such pair found then simply return the array.

### Input: 
An array, value, and size of the array

### Output: 
An array with two integers that add up to value

### Sample Input:
8

[1,21,3,14,5,60,7,6]

81

### Sample Output 
[21,60]