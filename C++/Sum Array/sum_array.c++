#include <iostream>
#include <algorithm>

using namespace std;

int* find_sum(int arr[], int value, int size) {
	
	sort(arr, arr + size);
	int start = 0, end = size - 1;

	while (start < end) {
		
		if (arr[start] + arr[end] == value) {
			arr[0] = arr[start];
			arr[1] = arr[end];
	 		return arr;
		}

		else if(arr[start] + arr[end] < value) 
			start++;
		else
			end--;
	}

	return arr;
}

int main(int argc, char* argv[]) {
	
	int size, value;
    cin >> size;

    int* arr = new int[size];
	for (int i = 0; i < size; i++) {
		cin >> arr[i];
	}

    cin >> value;
	
	int* res = find_sum(arr, value, size);
	for (int i = 0; i < 2; i++) {
		cout << *(res + i) << " ";
	}
	
    cout << endl;
	return 0;
}
