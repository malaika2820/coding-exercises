#include <iostream>

using namespace std;

int count_zeros(int n);

int main(int argc, char* argv[]) {
    int n;
    cin >> n;
    cout << count_zeros(n) << endl;
    return 0;
}

int count_zeros(int n) {
    int count = 0;
    int i = 5;
    while (n >= i) {
        count+= int(n / i);
        i*= 5;
    }
    return count;
}