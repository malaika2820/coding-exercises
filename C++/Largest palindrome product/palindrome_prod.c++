#include <iostream>

using namespace std;

bool is_palindrome(int n) {

	int n1 = n;
	int s = 0;
	
	while (n1 > 0) {
		s = s * 10 + n1 % 10;
		n1 /= 10;
	}

	return s == n ? true : false;
}

int palin_prod(int n) {
	
	int product = 0;
	
	for (int i = n; i > 1; i--) {
		for (int j = n; j >= (i - 20); j--) {
			if (is_palindrome(i * j)) {
				if (i * j > product)
					product = i * j;
			}
		}
	}

	return product;
}

int main(int argc, char* argv[]) {

	int n = 999;
	cout << palin_prod(n) << endl;

	return 0;
}
