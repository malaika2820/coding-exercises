#include <iostream>

using namespace std;

long long gcd(long long a, long long b) {
	
	if (a == 0)
		return b;
	if (b == 0)
		return a;
	return gcd(b, a % b);
}

long long smallest_multiple(int n) {
	
	long long lcm = 1;
	
	for (int i = 1; i <= n; i++) 
		lcm = (lcm * i) / (gcd(lcm, i));
	
	return lcm;
}

int main(int argc, char* argv[]) {

	//int n = 20;	
	int n = stoi(argv[1]);
	cout << smallest_multiple(n) << endl;

	return 0;
}
