A positive integer d is said to be a factor of another positive integer N, when N is divided by d, the remainder obtained is zero.

For example, for number 12, there are 6 factors 1, 2, 3, 4, 6, 12.

Every positive integer N has at least two factors, 1 and the number N itself.

Given a number N, the program prints the kth largest factor of N.

Sample Input :

N = 12
k = 3

Output : 4