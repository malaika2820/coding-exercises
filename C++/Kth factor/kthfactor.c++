#include <iostream>
#include <vector>

using namespace std;

vector<int> all_factors(int n);

vector<int> all_factors(int n) {
	vector<int> factors = {1};

	for (int i = 2; i <= n; i++) {
		if (n % i == 0)
			factors.push_back(i);
	}
	return factors;
}

int main(int argc, char* argv[]) {
	int k = stoi(argv[1]);
	int n = stoi(argv[2]);
	vector<int> factors = all_factors(n);
	
	cout << "The kth largest factor is ";

	if (k > factors.size()) {
		cout << "1" << endl;;
	}
	else {
	cout << factors[factors.size() - k] << endl;
	}
	return 0;
}