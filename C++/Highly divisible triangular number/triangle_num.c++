#include <iostream>

using namespace std;

long long triangle_sum(int n) {

	return n * (n + 1) / 2;
}

int num_divisors(long long n) {

	int count = 2;
	for (int i = 2; i < n; i++) {
		if (n % i == 0)
			count += 1;
	}

	return count;
}

int nth_triangle(int num) {

	int div_count = 0;
	int i = 1;

	while (div_count <= num) {
		
		div_count = num_divisors(triangle_sum(i));
		if (div_count > num)
			return i;

		i++;
	}

	return i;
}

int main(int argc, char* argv[]) {

	int n = 500;
	cout << nth_triangle(n) << endl;

	return 0;
}
