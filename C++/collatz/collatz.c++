#include <iostream>
#include <vector>
#include <iomanip>

using namespace std;

int next_term(int n) {
	return n % 2 == 0 ? n / 2 : 3 * n + 1;
}

vector<int> Collatz(int n){
	vector<int> series;
	
	for(; n != 4; n = next_term(n)) {
		series.push_back(n);
	}
	series.insert(series.end(), {4, 2, 1});
	return series;
}

int main(int argc, char* argv[]) {
	
	int START = stoi(argv[1]);

	vector<int> series = Collatz(START);
	
	for(const int c: series) {
		cout << setw(6) << c;
	}
	cout << endl;
	return 0;
}
