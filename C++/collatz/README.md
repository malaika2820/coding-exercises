Starting with any positive integer N, Collatz sequence is defined corresponding to n as the numbers formed by the following operations :

If n is even, then n = n / 2.
If n is odd, then n = 3*n + 1.
Repeat above steps, until it becomes 1

Sample Input : 3

Sample Output : 3, 10, 5, 16, 8, 4, 2, 1       


Sample Input : 6

Sample Output : 6, 3, 10, 5, 16, 8, 4, 2, 1