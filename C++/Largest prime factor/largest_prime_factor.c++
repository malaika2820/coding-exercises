#include <iostream>
#include <cmath>

using namespace std;

bool is_prime(long long n) {

	if (n <= 1 || n % 2 == 0 || n % 3 == 0)
		return false;

	int r = 5;
	while (r * r <= n) {
		if (n % r == 0)
			return false;
		r += 2;
		if (n % r == 0)
			return false;
		r += 4;
	}
	return  true;
}

long long largest_prime(long long n) {

	int prime_fac = 2;
	
	for (long long i = 2; i <= sqrt(n); i++) {
		if (n % i == 0 && is_prime(i))
			prime_fac = i;
	}

	return prime_fac;
}

int main(int argc, char* argv[]) {

	//long long n = 600851475143;
	long long n;
	cin >> n;

	cout << largest_prime(n) << endl;
	return 0;
}
