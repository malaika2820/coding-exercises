#include <iostream>
#include <stack>

using namespace std;

string decode_string(string seq);

int main(int argc, char* argv[]) {
    string seq;
    cin >> seq;
    
    cout << decode_string(seq) << endl;
    
    return 0;
}

string decode_string(string seq) {
    string result;
    stack<int> st;
    
    for (int i = 0; i <= seq.length(); i++) {
        
        st.push(i + 1);
        if (i == seq.length() || seq[i] == 'I') {
            
            while (!st.empty()) {
                result+= to_string(st.top());
                st.pop();
            }    
        }
    }
    return result;
}