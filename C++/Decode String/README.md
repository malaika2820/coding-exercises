Given a sequence consisting of 'I' and 'D' where 'I' denotes increasing sequence and 'D' denotes the descreasing sequence. The program decodes the given sequence to construct minimum number without repeated digits.


Sample Input 1:

IDIDII

Sample Output 1:

1325467



Sample Input 2:

DDDD

Sample Output 2:

54321