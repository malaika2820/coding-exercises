This problem is from Codechef (Bear and Extra Numbers)


A sequence is called nice if its elements are distinct consecutive numbers, possibly in changed order. For example, both (6, 7, 8) and (15, 13, 16, 14) are nice, while (4, 6), (4, 5, 5, 6) and (15, 16, 15) are not.

Input : Number of test cases, and for each test case, input N denoting the size of the new sequence and then enter the numbers in the sequence. 

Output : 
For each test case, output a single line containing one integer — a number that should be removed from the given sequence.


Sample input:
2
5
45 42 46 48 47
3
7 7 8


Output:
42
7

