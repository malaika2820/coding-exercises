#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

int remove_num(int n, vector<int> numbers);

int main(int argc, char* argv[]) {
	int T = stoi(argv[1]);
	
	for (int i = 1; i <= T; i++) {
	       int n;
	       cin >> n;
	       vector<int> numbers;
	       for (int j = 0; j < n; j++) {
		       cin >> numbers[j];
	       }
	       cout << remove_num(n, numbers) << endl;
	}
	return 0;
}
int remove_num(int n, vector<int> numbers) {
	sort(numbers.begin(), numbers.end());
	if (numbers[1] - numbers[0] != 1)
		return numbers[0];
	if (numbers[n - 1] - numbers[n - 2] != 1)
		return numbers[n - 1];
	for (int i = 1; i < n - 1; i++) {
		if (numbers[i] == numbers[i + 1])
			return numbers[i];
    }
}      			       
