#include <iostream>

using namespace std;

long long sum_squares(int n) {

	long long total = 0;
	for (int i = 1; i <= n; i++) {
		total += i * i;
	}

	return total;
}

long long square_sum(int n) {

	long long total = n * (n + 1) / 2;
	return total * total;
}

long long difference(int n) {

	long long sum_sq = sum_squares(n);
	long long sq_sum = square_sum(n);

	return abs(sum_sq - sq_sum);
}

int main(int argc, char* argv[]) {

	//int n = 100;
	int n = stoi(argv[1]);
	cout << difference(n) << endl;

	return 0;
}
