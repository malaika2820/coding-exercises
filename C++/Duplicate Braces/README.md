Given a string A denoting an expression. It contains the following operators ’+’, ‘-‘, ‘*’, ‘/’.

Check whether A has redundant braces or not.

Return 1 if A has redundant braces, else return 0.

Note: A will be always a valid expression.

Sample Input 1 : "((a + b))"

Sample Output 1 : 1

Sample Input 2 :  "(a + (a + b))"

Sample Output 2 : 0