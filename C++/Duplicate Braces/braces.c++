#include <iostream>
#include <stack>

using namespace std;

int check_redundant(string exp);

int main(int argc, char* argv[]) {
    
    string exp;
    getline(cin, exp);
    
    cout << check_redundant(exp) << endl;
    return 0;
}

int check_redundant(string exp) {
    stack<char> st;
    
    for (char ch : exp) {
        if (ch == ')') {
            char top = st.top();
            st.pop();
            
            int elements = 0;
            
            while (top != '(') {
                elements++;
                top = st.top();
                st.pop();
            }
            
            if (elements <= 1)
                return 1;
        }
        else
            st.push(ch);
    }
    
    return 0;
}