def check_prime(n : int) -> int:
    
    if n <= 1:
        return False
    if n in [2, 3, 5, 7] :
        return True
    if n % 2 == 0 or n % 3 == 0:
        return False
    r = 5
    while r * r <= n:
        if n % r == 0:
            return False
        r += 2
        if n % r == 0:
            return False
        r += 4
    return True

def getPrimoral(number : int) -> int:
    
    count, i, primorial = 0, 2, 1
    while count < number:
        if check_prime(i):
            primorial *= i 
            count += 1
        i += 1
    return primorial