A Primorial is a product of the first n prime numbers (e.g. 2 x 3 x 5 = 30). 
2, 3, 5, 7, 11, 13 are prime numbers.
If n was 3, you'd multiply 2 x 3 x 5 = 30 or Primorial = 30.

Write a program that returns the Primorial of a number.


Sample Input: 3 

Sample Output: 30
