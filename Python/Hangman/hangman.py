def process_letter(letter : str, word : str, guessed_word :str ) -> str:

    indexes = [i for i in range(len(word)) if word[i] == letter]
    new_word = ""

    for i in range(len(guessed_word)):
        if i in indexes:
            new_word+= letter
        else:
            new_word+= guessed_word[i]

    return new_word

def play(word : str):

    turns = 12
    guesses = 0
    guessed_word = "".join(["_" for i in word])
    letters_not_present = []

    while guesses < turns :
        guesses+= 1

        if guessed_word != word:
            letter = input("\nGuess a letter - ")
            if letter not in word:
                letters_not_present.append(letter)
                print("Not present - ", letters_not_present)
            else:
                guessed_word = process_letter(letter, word, guessed_word )
            print(guessed_word)
            print("Turns left - ", turns - guesses)
        else:
            print("You win")
            break


play('hangman')
