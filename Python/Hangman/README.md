This is a Python script of the classic game “Hangman”. 

The word to guess is represented by a row of dashes. If the player guess a letter which exists in the word, the script writes it in all its correct positions.  

The player has 12 turns to guess the word. The number of turns can be customized. 