Write a function that groups a string into parentheses cluster. Each cluster should be balanced.

Examples

SplitParenthesis("()()()") - ["()", "()", "()"]

SplitParenthesis("((()))") - ["((()))"]

SplitParenthesis("((()))(())()()(()())") - ["((()))", "(())", "()", "()", "(()())"]

SplitParenthesis("((())())(()(()()))") - ["((())())", "(()(()()))"]