def is_balanced(txt : str) -> bool:
    s = ""
    index = 0
    balanced = True
    while index < len(txt) and balanced:
        if txt[index] == "(":
            s+= txt[index]
        else:
            if len(s) == 0:
                balanced =  False
            else:
                s = s[0 : len(s) - 1 :]
        index+= 1
    return balanced and len(s) == 0

def SplitParenthesis(txt : str) -> []:
    s = ""
    arr = []
    for i in txt:
        s+= i
        if is_balanced(s):
            arr.append(s)
            s = "" 
    return arr