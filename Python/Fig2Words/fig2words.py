I_DENOMS = [10 ** 7, 10 ** 5, 1000, 1]
W_DENOMS = [10 ** 9, 10 ** 6, 1000, 1]

W_DENOM_NAMES = [" Billion ", " Million ", " Thousand ", ""]
I_DENOM_NAMES = [" Crore ", " Lakh ", " Thousand ", ""]

HUNDRED = " hundred"

Ws = dict(zip(W_DENOMS, W_DENOM_NAMES))
Is = dict(zip(I_DENOMS, I_DENOM_NAMES))

def convert2digits(n: int):
    upto20 =  ['zero', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine', 'ten', 'eleven',
                'twelve', 'thirteen', 'fourteen', 'fifteen', 'sixteen', 'seventeen', 'eighteen', 'nineteen' ]
    tens = ["", "", 'twenty', 'thirty', 'forty', 'fifty', 'sixty', 'seventy', 'eighty', 'ninety']

    if n < len(upto20):
        return upto20[n].capitalize()
    if n % 10 == 0:
        return tens[n // 10]
    return tens[n // 10] + " " + upto20[n % 10]

def convert3digits(n: int):
    if n < 100:
        return convert2digits(n)
    return fix_and(convert2digits(n // 100) + HUNDRED + convert2digits(n % 100))

def fix_and(s: str) -> str:
    if HUNDRED in s and not s.endswith(HUNDRED):
        return s.replace(HUNDRED, HUNDRED + " and ")
    return s

def split_into_denoms(amount: int, denoms: [int]) -> [(int, int)]:
    if len(denoms) == 1:
        return [(denoms[0], amount)]
    return [(denoms[0], amount // denoms[0])] + split_into_denoms(amount % denoms[0], denoms[1:])

def convert_denoms(splits: [(int, int)], names: {int: str}) -> [(str, str)]:
    return [(names[x[0]], x[1]) for x in splits]

def convert_multiples(splits:[(str, int)]) -> [(str, str)]:
    return [(convert3digits(x[1]), x[0]) for x in splits if x[1] != 0]

def combine(pieces: [(str, str)]) -> str:
    return ["".join(x + y) for x, y in pieces if len(x.strip()) != 0]

step1 = (split_into_denoms(4567123, W_DENOMS))
step2 = convert_denoms(step1, Ws)
step3 = convert_multiples(step2)
step4 = "".join(combine(step3))
print(step4.capitalize())

step1 = (split_into_denoms(4567123, I_DENOMS))
step2 = convert_denoms(step1, Is)
step3 = convert_multiples(step2)
step4 = "".join(combine(step3))
print(step4.capitalize())