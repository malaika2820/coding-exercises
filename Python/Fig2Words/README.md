Given a number in figures, the program will output the number in words, in bothe Indian and Western System of Numbering.

Constraints :
Upto 100 crore for Indian system
Upto 100 billion for Western system

Sample Input :

Input  : 4567123

Output : 

Four million five hundred and sixty seven thousand one hundred and twenty three

Forty five lakh sixty seven thousand one hundred and twenty three
