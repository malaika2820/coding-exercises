def is_letter(ch):
    return ch == "A" or ch == "B" or ch == "C"

def is_Invalid(string) -> bool:
    if len(string) % 2 == 0 and len(string) < 3:
        return True
    for i in range(len(string)):
        if i % 2 == 0 and is_letter(string[i]):
            return True
    return False

def evaluate_andORxor(string):
    if string[1] == "A":
        return (int(string[0]) and int(string[2]))

    if string[1] == "B":
        return (int(string[0]) or int(string[2]))

    if string[1] == "C":
        return (int(string[0]) ^ int(string[2]))

def Evaluating_String(string):
    if is_Invalid(string):
        return -1
    while len(string) > 1:
        string = string.replace(string[0:3] , str(evaluate_andORxor(string[0:3])))
    return int(string)


def test():
    s = input()
    print(Evaluating_String(s))