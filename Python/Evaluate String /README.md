Given a string consisting of only 0, 1, A, B, C where

A = AND
B = OR
C = XOR

Write a function to calculate the value of the string assuming no order of precedence and evaluation is done from left to right.

If the string is not valid then return -1.


Sample Input :

Input 1 : '1AA0'
Output 1 : -1

Input 2 : '1C1B1B0A0'
Output 2: