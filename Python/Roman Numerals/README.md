Given a Roman numeral, write a program which converts the Roman numeral to its decimal form. 

Sample Input :

Input 1 : CXCVII
Output 1 : 197

Input 2 : CCCXCIX
Output 2 : 399