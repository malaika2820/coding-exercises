Classic snake game made in python using turtle library 

The controls are :

A - left

W - up

D - right

S - down

The setup is 600 x 600 simple screen which displays the score and high score. The high score is reset once you close the terminal. As soon as you lose, the snake resets to default position to begin a new game. The speed of the snake increases as the game proceeds, and can be varied according to user. 
