#include <iostream>
#include <vector>

typedef long long int ll;

using namespace std;

int knapSack(int capacity, vector<int> values, vector<int> weights, int n) {
	
	vector<vector<int>>  K(n + 1, vector<int>(capacity + 1));
	
	for (int i = 0; i <= n; i++) {
		for (int w = 0; w <= capacity; w++) {
			if (i == 0 || w == 0)
				K[i][w] = 0;
			else if (weights[i - 1] <= w)
				K[i][w] = max(values[i - 1] + K[i - 1][w - weights[i - 1]], K[i - 1][w]);
			else
				K[i][w] = K[i - 1][w];
		}
	}

	return K[n][capacity];
}

int main(int argc, char* argv[]) {
	
	int n, capacity;
	cin >> capacity >> n;

	vector<int> values(n);
	vector<int> weights(n);
	int temp;
	
	for (size_t i = 0; i < n; i++) {
		cin >> temp;
		values[i] = weights[i] = temp;
	}
	cout << knapSack(capacity, values, weights, n) << endl;

	return 0;
}
