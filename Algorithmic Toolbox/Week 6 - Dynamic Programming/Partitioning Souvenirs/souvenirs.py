import numpy

def partitions(total_wt : int, n : int, items : [int]) -> int:

    if n < 3 :
        return 0

    if sum(items) % 3 != 0:
        return 0
    
    count = 0 
    value = numpy.zeros((total_wt + 1, n + 1))
    
    for i in range(1, total_wt + 1):
        for j in range(1, n + 1):
            value[i][j] = value[i][j - 1]
            
            if items[j - 1] <= i:
                temp = value[i - items[j - 1]][j - 1] + items[j - 1]
                
                if temp > value[i][j]:
                    value[i][j] = temp
            
            if value[i][j] == total_wt: 
                count += 1
    
    return 0 if count < 3 else 1

if __name__ == '__main__':
    
    n = int(input())
    item_weights = list(map(int, input().split()))
    total_weight = sum(item_weights)
    
    print(partitions(total_weight // 3, n, item_weights))
