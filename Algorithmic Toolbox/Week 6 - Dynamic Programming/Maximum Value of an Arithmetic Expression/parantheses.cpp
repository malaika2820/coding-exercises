#include <iostream>
#include <cstring>
#include <climits>

typedef   long long int ll ;

using namespace std;

ll make_operation(ll operand_1, ll operand_2, char op) {
	
	if (op == '*')
		return operand_1 * operand_2;
	if (op == '+')
		return operand_1 + operand_2;
	if (op == '-')
		return operand_1 - operand_2;
	
	return operand_1 / operand_2;
}

ll maximize(const string expression) {
	
	int num_operands = (expression.size() + 1) / 2;

	ll mini[num_operands][num_operands];
	ll maxi[num_operands][num_operands];

	memset(mini, 0, sizeof(mini)); 
	memset(maxi, 0, sizeof(maxi));  
	
	for (int i = 0; i < num_operands; i++) {

		mini[i][i] = stoll(expression.substr(2 * i, 1));
		maxi[i][i] = stoll(expression.substr(2 * i, 1));
	}

	for (int s = 0; s < num_operands - 1; s++) {
		for (int i = 0; i < num_operands - s - 1; i++) {
			
			int j = i + s + 1;
			ll min_value = LLONG_MAX;
			ll max_value = LLONG_MIN;

			for (int k = i; k < j; k++) {
				ll a = make_operation(mini[i][k], mini[k + 1][j], expression[2 * k + 1]);
				ll b = make_operation(mini[i][k], maxi[k + 1][j], expression[2 * k + 1]);
				ll c = make_operation(maxi[i][k], mini[k + 1][j], expression[2 * k + 1]);
				ll d = make_operation(maxi[i][k], maxi[k + 1][j], expression[2 * k + 1]);

				min_value = min(min_value, min(a, min(b, min(c, d))));
				max_value = max(max_value, max(a, max(b, max(c, d))));

			}
			mini[i][j] = min_value;
			maxi[i][j] = max_value;
		}
	}

	return maxi[0][num_operands - 1];
}

int main(int argc, char* argv[]) {
	
	string expresion;
	cin >> expresion;

	cout << maximize(expresion) << endl;
	return 0;
}
