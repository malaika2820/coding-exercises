#include <iostream>
#include <vector>

using namespace std;

int compute_min_refills(int dist, int tank, vector<int> stops) {

	if (stops[0] > tank)
		return -1;
	
	stops.push_back(dist);
	int can_go_to = tank;
	int count = 0;

	for (int i = 0; i < stops.size(); i++) {
		
		if (stops[i] <= can_go_to)
			continue;
		else {
			can_go_to = stops[i - 1] + tank;
			count += 1;
			if (can_go_to < stops[i])
				return -1;
		}
	}
	
	return count;
}

int main(int argc, char* argv[]) {
	
    	int d = 0;
    	cin >> d;
    		
	int m = 0;
    	cin >> m;
    	
	int n = 0;
    	cin >> n;
	
	vector<int> stops(n);	
	for (int i = 0; i < n; i++) {
		cin >> stops[i];
	}
	
	cout << compute_min_refills(d, m, stops) << endl;
	return 0;
}
