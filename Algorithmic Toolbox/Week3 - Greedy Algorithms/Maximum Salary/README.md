## Problem Introduction
As the last question of a successful interview, your boss gives you a few pieces of paper with numbers on it and asks you to compose a largest number from these numbers. The resulting number is going to be your salary, so you are very much interested in maximizing this number. How can you do this?

## Problem Description
Compose the largest number out of a set of integers.

## Input Format:
The first line of the input contains an integer 𝑛. The second line contains integers 𝑎1, 𝑎2, . . . , 𝑎𝑛.

## Constraints:
1 ≤ 𝑛 ≤ 100; 1 ≤ 𝑎𝑖 ≤ 10 ^ 3 for all 1 ≤ 𝑖 ≤ 𝑛.

## Output Format:
Output the largest number that can be composed out of 𝑎1, 𝑎2, . . . , 𝑎𝑛.

## Sample 1-
### Input:
2

21 2

### Output:
221
## Sample 2-
### Input:
5

9 4 6 1 9

### Output:
99641

## Sample 3-
### Input:
3

23 39 92

### Output:

923923