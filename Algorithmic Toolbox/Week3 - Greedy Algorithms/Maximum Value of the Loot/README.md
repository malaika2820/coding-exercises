## Problem Description
The goal of this code problem is to implement an algorithm for the fractional knapsack problem.

## Input Format:
The first line of the input contains the number 𝑛 of items and the capacity 𝑊 of a knapsack.
The next 𝑛 lines define the values and weights of the items. The 𝑖-th line contains integers 𝑣𝑖 and 𝑤𝑖—the value and the weight of 𝑖-th item,respectively.

## Constraints:
1 ≤ 𝑛 ≤ 10 ^ 3, 0 ≤ 𝑊 ≤ 2 · 10 ^ 6; 0 ≤ 𝑣𝑖 ≤ 2 · 10^6, 0 < 𝑤𝑖 ≤ 2 · 10 ^ 6 for all 1 ≤ 𝑖 ≤ 𝑛. 
All the numbers are integers.

## Output Format:
Output the maximal value of fractions of items that fit into the knapsack. 
## Sample 1-
### Input:
3 50

60 20

100 50

120 30

### Output:
180.0000

To achieve the value 180, we take the first item and the third item into the bag.

## Sample 2-
### Input:
1 10

500 30

### Output:
166.6667