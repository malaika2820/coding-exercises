def get_optimal_value(n : int, capacity : int, m :[]) -> float:

    m = sorted(m, key = lambda x : x[2], reverse = True)
     
    value = 0.0

    for i in m:
        
        if i[1] <= capacity:
            count = i[1]
        else:
            count = capacity
        
        capacity -= count
        value += count * i[2]
    
    return value

if __name__ == "__main__":
    
    n, capacity = map(int, input().split())
    m = []
    
    for i in range(n):
        k, v = map(int, input().split())
        m.append((k, v, k/v))

    opt_value = get_optimal_value(n, capacity, m)
    
    print("{:.4f}".format(opt_value))

