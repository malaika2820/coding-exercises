#include <iostream>

using namespace std;

int get_change(int m) {
	
	int tens = m / 10;
	int fives = (m % 10) / 5;
	int ones = (m % 10) % 5;
	
	return tens + fives + ones;
}


int main(int argc, char* argv[]) {

	int m;
	cin >> m;

	cout << get_change(m) << endl;

	return 0;
}
