#include <iostream>
#include <vector>

using namespace std;

int longest_subseq(vector<int> a, vector<int> b, int n, int m) {
	
	int seq[n + 1][m + 1];
	
	for (int i = 0; i <= n; i++)
		seq[i][0] = 0;
	
	for (int i = 0; i <= m; i++)
		seq[0][i] = 0;

	for (int i = 1; i <= n; i++) {
		for (int j = 1; j <= m; j++) {
			if (a[i - 1] == b[j - 1])
				seq[i][j] = 1 + seq[i - 1][j - 1];
			else
				seq[i][j] = max(seq[i - 1][j], seq[i][j - 1]);
		}
	}
	return seq[n][m];
}

int main(int argc, char* argv[]) {

	int t;

	int n;
	cin >> n;
	vector<int> a;
	for (int i = 0; i < n; i++) {
		cin >> t;
		a.push_back(t);
	}

	int m;
	cin >> m;
	vector<int> b;
	for (int i = 0; i < m; i++) {
		cin >> t;
		b.push_back(t);
	}

	cout << longest_subseq(a, b, n, m) << endl;
	
	return 0;
}
