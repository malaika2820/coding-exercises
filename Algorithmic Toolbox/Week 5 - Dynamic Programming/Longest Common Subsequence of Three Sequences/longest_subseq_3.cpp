#include<iostream>
#include <algorithm>
#include <vector> 

using namespace std; 

int longest_subseq(vector<int> X, vector<int> Y, vector<int> Z, int m, int n, int o) { 
    
	int L[m + 1][n + 1][o + 1]; 

    	for (int i = 0; i <= m; i++) { 
        	for (int j = 0; j <= n; j++) { 
            		for (int k = 0; k <= o; k++) { 
                		if (i == 0 || j == 0||k==0) 
                    			L[i][j][k] = 0; 
  
                		else if (X[i-1] == Y[j-1] && X[i-1]==Z[k-1]) 
                    			L[i][j][k] = L[i-1][j-1][k-1] + 1; 
  
                		else
                    			L[i][j][k] = max(max(L[i-1][j][k], L[i][j-1][k]), L[i][j][k-1]);
			}
		}
	}
	return L[m][n][o];
}	
 
int main(int argc, char* argv[]) {

    vector<int> X; 
    vector<int> Y;  
    vector<int> Z;  
    int t;

    int m;
    cin >> m;
    for (int i = 0; i < m; i++) {
	    cin >> t;
	    X.push_back(t);
    }

    int n;
    cin >> n;
    for (int i = 0; i < n; i++) {
	   cin >> t;
	   Y.push_back(t);
    } 
    
    int o;
    cin >> o;
    for (int i = 0; i < o; i++) {
	   cin >> t;
	   Z.push_back(t);
    } 
  
    cout << longest_subseq(X, Y, Z, m, n, o); 
  
    return 0; 
} 
