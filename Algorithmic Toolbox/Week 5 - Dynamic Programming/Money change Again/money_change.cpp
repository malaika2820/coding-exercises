#include <iostream>
#include <vector>

using namespace std;

int get_change(int money, vector<int> coins) {

	int min_coins[money + 1];
	min_coins[0] =  0;
	int num_coins = 0;

	for (int i = 1; i <= money; i++) {
		
		min_coins[i] = money;
		
		for (int j : coins) {
			if (i >= j) {
				num_coins = min_coins[i - j] + 1;
				if (num_coins < min_coins[i])
					min_coins[i] = num_coins;
			}
		}
	}
	return min_coins[money];
}

int main(int argc, char* argv[]) {
	
	int money;
	cin >> money;
	vector<int> coins = {1, 3, 4};

	cout << get_change(money, coins) << endl;

	return 0;
}
