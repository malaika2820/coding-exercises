#include <iostream>
#include <vector>

using namespace std;

int fibonacci_sum_naive(long long n) {
    if (n <= 1)
        return n;

    long long previous = 0;
    long long current  = 1;
    long long sum      = 1;

    for (long long i = 0; i < n - 1; ++i) {
        long long tmp_previous = previous;
        previous = current;
        current = tmp_previous + current;
        sum += current;
    }

    return sum % 10;
}

int fibonacci_sum(long long n) {
	vector<long long> seq = {0, 1};
	long long sum = 1;
	for (long long i = 2; i <= n; i++) {
		seq.push_back(seq[i-1] + seq[i-2]);
		sum += seq[i];
	}
	return sum % 10;
}

typedef long long int ll;

ll Periodic(ll m) {
	ll a = 0, b = 1, c = a + b;
	for (int i = 0; i < m*m; i++) {
		c = (a + b) % m;
		a = b;
		b = c;
		if (a == 0 && b == 1)
			return i + 1;
	}
}
int Solve(ll n, ll m) {
	
	ll remainder = n % Periodic(m);
	ll first = 0;
	ll second = 1;
	ll res = remainder;

	for (int i = 1; i < remainder; i++) {
		res = (first + second) % m;
		first = second;
		second = res;
	}

	return res % m;
}

ll Huge_Fibonacci(ll n) {
	int Last_Digit_Of_nPlus2 = Solve(n + 2, 10);
	int Last_Digit_Of_2 = Solve(2, 10);

	if (Last_Digit_Of_nPlus2 >= Last_Digit_Of_2)
		return (Last_Digit_Of_nPlus2 - Last_Digit_Of_2);
	else
		return ((10 + Last_Digit_Of_nPlus2) - Last_Digit_Of_2);
}
int main(int argc, char* argv[]) {
	ll n;
	cin >> n;
	cout << Huge_Fibonacci(n) << endl;

	return 0;
}
