#include <iostream>
#include <vector>

using namespace std;

int get_fibonacci_mod_small(int n, int m)
{
    	int fib[n + 1];
    	fib[0] = 0;
    	fib[1] = 1;
    	for (int i = 2; i <= n; i++) {
        	fib[i] = (fib[i - 1] + fib[i - 2]) % m;
    	}

    return fib[n];
}

int get_pisano_period(int m) {
    	if (m == 1) 
        	return 0;
    
	int first = -1;
    	int second = -1;
    	int i = 2;
    	while (true) {
        	second = get_fibonacci_mod_small(i, m);
        	if (second == 1 && first == 0)
            		return i - 1;
        
        	first = second;
        	i += 1;
    	}
}

long long fib_huge(long long n, long long m) {
    
	int pisano_length = get_pisano_period(m);
    	n %= pisano_length;

	return get_fibonacci_mod_small(n, m);
}

int fibonacci_sum(long long from, long long to) {
    	
	return (fib_huge(to + 2, 10) - 1) - (fib_huge(from + 1, 10) - 1);
}

int main(int argc, char* argv[]) {
    
	long long from, to;
   	cin >> from >> to;
    	
	int sum = fibonacci_sum(from, to);
    	int value = sum < 0 ? (sum + 10) : sum;
    	cout << value;

	return 0;
}
