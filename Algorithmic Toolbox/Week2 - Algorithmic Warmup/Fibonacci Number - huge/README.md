## Problem Description
Given two integers 𝑛 and 𝑚, output 𝐹𝑛 mod 𝑚 (that is, the remainder of 𝐹𝑛 when divided by 𝑚).

## Input Format: 
The input consists of two integers 𝑛 and 𝑚 given on the same line (separated by a space).

## Constraints: 
1 ≤ 𝑛 ≤ 10 ^ 14, 2 ≤ 𝑚 ≤ 10 ^ 3.

## Output Format: 
Output 𝐹𝑛 mod 𝑚.

## Sample 1-
### Input:
239 1000

### Output:
161

𝐹239 mod 1000 = 39679027332006820581608740953902289877834488152161 (mod 1000) = 161.

## Sample 2-
### Input:
2816213588 239

### Output:

151

𝐹2816213588 mod 239 = 151.