#include <iostream>
#include <cassert>
#include <vector>

using namespace std;

int fibonacci_naive(int n) {
    if (n <= 1)
        return n;

    return fibonacci_naive(n - 1) + fibonacci_naive(n - 2);
}

int fibonacci_fast(int n) {
	
	vector<int> seq(n + 1);
	seq[0] = 0;
	seq[1] = 1;

    	for (int i = 2; i <= n; i++) {
		seq[i] = seq[i - 1] + seq[i - 2];
    	}
    	return seq[n];
}

void test_solution() {
    assert(fibonacci_fast(3) == 2);
    assert(fibonacci_fast(10) == 55);
    for (int n = 0; n < 20; ++n)
        assert(fibonacci_fast(n) == fibonacci_naive(n));
}

int main(int argc, char* argv[]) {
    int n = 0;
    cin >> n;

    //std::cout << fibonacci_naive(n) << '\n';
    //test_solution();
    cout << fibonacci_fast(n) << endl;
    return 0;
}
