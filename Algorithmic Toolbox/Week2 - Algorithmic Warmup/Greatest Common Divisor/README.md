The greatest common divisor GCD(𝑎, 𝑏) of two non-negative integers 𝑎 and 𝑏 (which are not both equal to 0) is the greatest integer 𝑑 that divides both 𝑎 and 𝑏. Your goal in this problem is to implement the Euclidean algorithm for computing the greatest common divisor.


Problem Description

**Task**: Given two integers 𝑎 and 𝑏, find their greatest common divisor.

**Input Format**: The two integers 𝑎, 𝑏 are given in the same line separated by space.

**Constraints**: 1 ≤ 𝑎, 𝑏 ≤ 2 · 10^9.

**Output Format**: Output GCD(𝑎, 𝑏).

**Sample 1:**

Input:

18 35

Output:

1

**Sample 2:**

Input:

28851538 1183019

Output:

17657