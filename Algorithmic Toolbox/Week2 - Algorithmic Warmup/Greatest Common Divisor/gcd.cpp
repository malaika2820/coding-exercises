#include <iostream>

using namespace std;

int gcd_naive(int a, int b) {
  int current_gcd = 1;
  for (int d = 2; d <= a && d <= b; d++) {
    if (a % d == 0 && b % d == 0) {
      if (d > current_gcd) {
        current_gcd = d;
      }
    }
  }
  return current_gcd;
}

long long GCD(long long a, long long b) {
	if (a == 0)
		return b;
	if (b == 0)
		return a;
	return GCD(b % a, a);
}

int main(int argc, char* argv[]) {
  	
	long long a, b;
  	cin >> a >> b;
  	
	//cout << gcd_naive(a, b) << endl;
  	cout << GCD(a, b) << endl;
  	
	return 0;
}
