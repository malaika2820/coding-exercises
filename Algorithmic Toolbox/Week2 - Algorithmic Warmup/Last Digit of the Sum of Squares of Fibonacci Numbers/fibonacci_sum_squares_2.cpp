#include <iostream>

typedef long long int ll;

using namespace std;

ll Periodic(ll m) {
	ll a = 0, b = 1, c = a + b;
	
	for (int i = 0; i < m*m; i++) {
		c = (a + b) % m;
		a = b;
		b = c;
		if (a == 0 && b == 1)
			return i + 1;
	}
}
int solve(ll n, ll m) {
	ll remainder = n % Periodic(m);

	ll first = 0;
	ll second = 1;

	ll res = remainder;

	for (int i = 1; i < remainder; i++) {
		res = (first + second) % m;
		first = second;
		second = res;
	}

	return res % m;
}

int main(int argc, char* argv[]) {
	
	ll n; 
	cin >> n;
	
	cout << (solve(n + 1, 10) * solve(n, 10)) % 10 << endl;
	
	return 0;
}
