## Problem Description

Compute the last digit of 𝐹1 ^ 2 + F2 ^ 2 + ... + Fn ^ 2

## Input Format:
Integer 𝑛.

## Constraints: 
0 ≤ 𝑛 ≤ 10 ^ 14.

## Output Format:
Single Integer

## Sample 1-
### Input:
7

### Output:
3

F7 = 0 + 1 + 1 + 4 + 9 + 25 + 64 + 169 = 273.

## Sample 2-
### Input:

73

### Output:

1

73 = 1052478208141359608061842155201.

## Sample 3-
### Input:
1234567890

### Output:
0