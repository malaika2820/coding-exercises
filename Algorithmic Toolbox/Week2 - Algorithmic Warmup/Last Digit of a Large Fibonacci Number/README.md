## Problem Introduction
Your goal in this problem is to find the last digit of 𝑛-th Fibonacci number.

## Problem Description
Given an integer 𝑛, find the last digit of the 𝑛th Fibonacci number 𝐹𝑛 (that is, 𝐹𝑛 mod 10).

## Input Format: 
The input consists of a single integer 𝑛.

## Constraints: 
0 ≤ 𝑛 ≤ 10 ^ 7.

## Output Format: 
Output the last digit of 𝐹𝑛.

## Sample 1:
### Input:
3
### Output:
2

𝐹3 = 2.
## Sample 2:
### Input:
331

### Output:

9

𝐹331 = 668996615388005031531000081241745415306766517246774551964595292186469.