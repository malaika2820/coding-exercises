#include <iostream>
#include <vector>

using namespace std;

int get_fibonacci_last_digit_naive(int n) {
    if (n <= 1)
        return n;

    int previous = 0;
    int current  = 1;

    for (int i = 0; i < n - 1; ++i) {
        int tmp_previous = previous;
        previous = current;
        current = tmp_previous + current;
    }

    return current % 10;
}
int fib_last_digit(int n) {
	
	vector<long long> seq(n + 1);
	seq[0] = 0;
	seq[1] = 1;

	for (int i = 2; i <= n; i++) {
		seq[i] = ((seq[i-1]) % 10 + (seq[i-2]) % 10) % 10;
	}
	return seq[n];
}

int main(int argc, char* argv[]) {
    int n;
    cin >> n;
    cout << fib_last_digit(n) << endl;
    return 0;
    }
