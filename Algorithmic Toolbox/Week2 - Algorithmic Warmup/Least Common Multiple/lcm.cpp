#include <iostream>

using namespace std;

long long gcd(long long a, long long b) {
	if (a == 0)
		return b;
	if (b == 0)
		return a;
	return gcd(b % a, a);
}

long long lcm_naive(int a, int b) {
  for (long l = 1; l <= (long long) a * b; ++l)
    if (l % a == 0 && l % b == 0)
      return l;

  return (long long) a * b;
}

long long lcm(long long a, long long b) {
	return (a * b)/gcd(a, b);
}


int main(int argc, char* argv[]) {
  	long long a, b;
  	cin >> a >> b;
  	//cout << lcm_naive(a, b) << endl;
  	cout << LCM(a, b) << endl;
  	
	return 0;
}
