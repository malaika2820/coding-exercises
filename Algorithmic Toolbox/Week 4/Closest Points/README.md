## Problem Description

## Task: 
Given 𝑛 points on a plane, find the smallest distance between a pair of two (different) points. Recall that the distance between points (𝑥1, 𝑦1) and (𝑥2, 𝑦2) is equal to √︀ (𝑥1 − 𝑥2)2 + (𝑦1 − 𝑦2)2.

## Input Format: 
The first line contains the number 𝑛 of points. Each of the following 𝑛 lines defines a point (𝑥𝑖, 𝑦𝑖).

## Constraints: 
2 ≤ 𝑛 ≤ 105; −109 ≤ 𝑥𝑖, 𝑦𝑖 ≤ 109 are integers.

## Output Format:
Output the minimum distance. The absolute value of the difference between the answer of your program and the optimal value should be at most 10−3. To ensure this, output your answer with at least four digits after the decimal point (otherwise your answer, while being computed correctly, can turn out to be wrong because of rounding issues).

## Sample 1.

### Input:
2

0 0

3 4
### Output:
5.0
There are only two points here. The distance between them is 5.

## Sample 2.

### Input:
4

7 7

1 100

4 8

7 7

### Output:
0.0

## Sample 3.
### Input:
11

4 4

-2 -2

-3 -4

-1 3

2 3

-4 0

1 1

-1 -1

3 -1

-4 2

-2 4

### Output:
1.414213