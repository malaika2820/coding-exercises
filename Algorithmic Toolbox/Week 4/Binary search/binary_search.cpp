#include <iostream>
#include <cassert>
#include <vector>

using namespace std;

int binary_search(vector<int> Arr, int n, int Target) {
	int l = 0, r = n - 1;
	while (l <= r) {
		int mid = (l + r) / 2;
		if (Arr[mid] == Target)
			return mid;
		else if (Arr[mid] < Target) {
			l = mid + 1; 
		}
		else if (Arr[mid] > Target) {
			r = mid - 1;
		}
	}
	return -1; 
}

int linear_search(const vector<int> &a, int x) {
  	for (size_t i = 0; i < a.size(); ++i) {
    		if (a[i] == x) 
			return i;
  	}
  	return -1;
}

int main(int argc, char* argv[]) {
  	int n;
  	cin >> n;
  	vector<int> a(n);
  	for (size_t i = 0; i < a.size(); i++) {
    		cin >> a[i];
  	}
  	int m;
  	cin >> m;
  	vector<int> b(m);
  	for (int i = 0; i < m; i++) {
 		cin >> b[i];
  	}
  	for (int i = 0; i < m; i++) {
    		cout << binary_search(a, n, b[i]) << " ";
    		//cout << linear_search(a, b[i]) << ' ';
  	}
	return 0;
}
