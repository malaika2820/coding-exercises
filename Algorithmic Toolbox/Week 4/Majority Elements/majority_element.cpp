#include <algorithm>
#include <iostream>
#include <vector>

using namespace std;

int get_majority_element(vector<int> &a) {
	
	if (a.size() == 0) 
		return -1;
  	
	if (a.size() == 1) 
		return a[0];
  	
	sort(a.begin(), a.end());
	
	int major = a[a.size() / 2];
	int count = 0;
	
	for (int i = 0; i < a.size(); i++) {
		if (a[i] == major)
			count++;
	}
	
	if (count > a.size() / 2)
		return 1;
	else
		return 0;	

}

int main(int argc, char* argv[]) {
  	
	int n;
  	cin >> n;
  	vector<int> a(n);
  	for (size_t i = 0; i < a.size(); ++i) {
    		cin >> a[i];
  	}
  
	cout << (get_majority_element(a)) << endl;
	
	return 0;
}
