#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

long long max_pairwise_prod(int n, vector<int> num) {
	sort(num.begin(), num.end());
	return (long long)num[n - 1] * (long long)num[n - 2];
}

int main(int argc, char* argv[]) {
	int n;
	cin >> n;
	vector<int> num;
	for (int i = 0; i < n; i++) {
		cin >> num[i];
	}
	cout << max_pairwise_prod(n, num);
	return 0;
}
