Maximum Pairwise Product Problem:
Given a sequence of n non-negative integers to find the maximum pairwise product. 

**Input**: A sequence of n non-negative integers.

**Input format**: The first line contains an integer n. 
The next line contains n non-negative integers (a1, a2, ..., an) (separated by spaces).

**Output**: The maximum value that can be obtained by multiplying two different elements from the sequence.

**Output format**: The maximum pairwise product.

**Constraints**: 2 <n < 2 * 10^5; 0 < a1, a2, ..., an < 2 * 10^5.

**Sample 1-**

Input:

3

1 2 3

Output:

6

**Sample 2-**

Input:

10

7 5 14 2 8 8 10 1 2 3

Output:

140