import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation

ALIVE = 1
DEAD = 0
vals = [ALIVE, DEAD]

def randomGrid(N):
    return np.random.choice(vals, N * N).reshape(N, N)

def check_neighbours(grid, N, i, j):
    return int(grid[i, (j - 1) % N] + grid[i, (j + 1) % N] +
                 grid[(i - 1) % N, j] + grid[(i + 1) % N, j] +
                 grid[(i - 1) % N, (j - 1) % N] + grid[(i - 1) % N, (j + 1) % N] +
                 grid[(i + 1) % N, (j - 1) % N] + grid[(i + 1) % N, (j + 1) % N])

def update(frameNUM, img, grid, N):
    newGrid = grid.copy()
    for i in range(N):
        for j in range(N):
            total = check_neighbours(grid, N, i, j)
            if grid[i, j]  == ALIVE:
                if (total < 2) or (total > 3):
                    newGrid[i, j] = DEAD
            elif total == 3:
                newGrid[i, j] = ALIVE

    img.set_data(newGrid)
    grid[:] = newGrid[:]
    return img,

def play():

    N = 100
    updateInterval = 50
    grid = np.array([])
    grid = randomGrid(N)

    fig, ax = plt.subplots()
    img = ax.imshow(grid, interpolation='nearest')
    ani = animation.FuncAnimation(fig, update, fargs=(img, grid, N, ),
                                  frames = 10,
                                  interval = updateInterval,
                                  save_count = 50)
    plt.show()

play()