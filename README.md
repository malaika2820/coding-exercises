# Coding Exercises

Coding questions in Python and C++

Problem statements along with solutions of questions I've solved in Python and C++. For some problems, there are multiple solutions and solved in both Python and C++.

C++ problems :
1. Duplicate Braces
2. Kth factor
3. Trailing Zeros
4. Collatz
5. Nice sequence
6. Multiples of 3 and 5
7. Even Fibonacci numbers
8. Largest prime factor
9. Largest palindrome product
10. Smallest multiple
11. Sum-square difference
12. 10,001st prime
13. Special Pythagorean triplet
14. Summation of Primes
15. Highly divisble triangle number

Python problems :
1. balanced parenthesis
2. Evaluate String
3. Fig2Words
4. Hangman
5. primorial
6. ranking_students
7. Roman numerals
8. snakeGame
9. Conway's game of Life
10. Largest product in a series
